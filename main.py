# encoding:utf-8
import warnings

warnings.filterwarnings("ignore")
import torch
import numpy as np
import time
from tqdm import tqdm
from utils.config import Config
from model.sgcnhr import SGCNHR
from utils.util import Helper
from utils.dataset import Dataset
import matplotlib.pyplot as plt
import os
from utils.parser import parse_args


def training(model, graph, train_loader, epoch_id, optimizer):
    losses = []
    for u, pi_ni in tqdm(train_loader, "training epoch: {0}".format(str(epoch_id))):
        users = u
        pos_items = pi_ni[:, 0]
        neg_items = pi_ni[:, 1]
        u_g_embeddings, pos_i_g_embeddings, neg_i_g_embeddings = model(graph, users, pos_items, neg_items)
        loss = model.create_bpr_loss(u_g_embeddings, pos_i_g_embeddings, neg_i_g_embeddings)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        losses.append(loss.item())
    return np.mean(losses)


def evaluation(model, graph, helper, epoch_id):
    res, res_hash = helper.eval_batch_all_rating(model, list(helper.dataset.test_items.keys()), graph, epoch_id)
    return res, res_hash


def draw(title, y_label, x_label, file_path, steps, metrics):
    plt.plot(steps, metrics)
    plt.title(title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.savefig(file_path)
    plt.close()


def draw_loss(file_path, losses):
    steps = [i for i in range(len(losses))]
    draw(title='loss curve', y_label='loss value', x_label='epoch', file_path=file_path, steps=steps, metrics=losses)


def draw_metrics(file_path, hrs, ndcgs, hrs_hash, ndcgs_hash):
    steps = [i for i in range(len(hrs))]
    dic = {0: '5', 1: "10", 2: '20', 3: '50', 4: '100'}
    for i in range(len(hrs[0])):
        draw(title='hr curve', y_label='hr value', x_label='epoch', file_path=file_path + "%s_hr.jpg" % dic[i], steps=steps, metrics=np.array(hrs)[:, i])
        draw(title='ndcg curve', y_label='ndcg value', x_label='epoch', file_path=file_path + "%s_ndcg.jpg" % dic[i], steps=steps, metrics=np.array(ndcgs)[:, i])
        draw(title='hr curve with hash', y_label='hr value', x_label='epoch', file_path=file_path + "%s_hr_hash.jpg" % dic[i], steps=steps, metrics=np.array(hrs_hash)[:, i])
        draw(title='ndcg curve with hash', y_label='ndcg value', x_label='epoch', file_path=file_path + "%s_ndcg_hash.jpg" % dic[i], steps=steps, metrics=np.array(ndcgs_hash)[:, i])


def save_result(filepath, results):
    with open(filepath, 'w+') as f:
        for item in results:
            f.write(item + '\n')
        f.close()


def save_model(model, path):
    torch.save(model, path)


def load_model(path):
    model = torch.load(path)
    return model


def saveEmbeddings(model, base_path):
    np.save(base_path + "/userEmbeddings.npy", model.user_embeddings.detach().cpu().numpy())
    np.save(base_path + "/itemEmbeddings.npy", model.item_embeddings.detach().cpu().numpy())
    np.save(base_path + "/userHashCodes.npy", torch.sign(model.user_embeddings).detach().cpu().numpy())
    np.save(base_path + "/itemHashCodes.npy", torch.sign(model.item_embeddings).detach().cpu().numpy())


if __name__ == '__main__':
    args = parse_args()
    config = Config(args)
    dataset = Dataset(config)
    helper = Helper(config, dataset)
    num_users, num_items = dataset.num_users, dataset.num_items
    print("num_users = %d, num_items = %d" % (num_users, num_items))
    print("avg_user_mean = %.2f, avg_item_user = %.2f" % (len(dataset.user_item_interactions) / num_users, len(dataset.user_item_interactions) / num_items))
    results = []
    print(str(args))
    results.append(str(args))
    best_hr = 0
    best_ndcg = 0
    base_path = "./results/save_models/" + config.dataset_path.split("/")[2] + "/" + time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
    graph = dataset.graph
    graph = graph.to(config.gpu)
    if not os.path.exists(base_path):
        os.makedirs(base_path)
    if config.use_pretrain_model and os.path.exists(base_path + "/best_model.pkl"):
        sgcnhr = load_model(base_path + "/best_model.pkl").to(config.gpu)
        print("加载预训练模型！")
    else:
        sgcnhr = SGCNHR(graph, config.embedding_size, config.embedding_size, config.drop_ratio, config.n_layer).to(config.gpu)

    losses = []
    hrs, ndcgs, hrs_hash, ndcgs_hash = [], [], [], []
    early_stop_step, cur_stop_step = 5, 0
    learning_rate = config.lr
    for epoch in range(config.epoch):
        # 模型训练
        t1 = time.time()
        sgcnhr.train().to(config.gpu)
        optimizer = torch.optim.Adam(sgcnhr.parameters(), learning_rate)
        loss = training(sgcnhr, graph, dataset.get_user_data_loader(config.batch_size), epoch, optimizer)
        losses.append(loss)
        print("Training time is: [%.2f s], loss is: [%.2f]" % (time.time() - t1, loss))

        # 模型评估
        t2 = time.time()
        sgcnhr.eval().to(config.gpu)
        res, res_hash = evaluation(sgcnhr, graph, helper, epoch)
        print("Evaluation time is: [%.2f s]" % (time.time() - t2))
        cur_hr = res['hit_ratio'][2]
        cur_ndcg = res['ndcg'][2]
        cur_hr_hash = res_hash['hit_ratio'][2]
        cur_ndcg_hash = res_hash['ndcg'][2]

        hrs.append(res['hit_ratio'])
        ndcgs.append(res['ndcg'])
        hrs_hash.append(res_hash['hit_ratio'])
        ndcgs_hash.append(res_hash['ndcg'])

        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   evaluation results >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        print("嵌入向量评估结果：", res)
        print("哈希码评估结果：", res_hash)
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   evaluation results >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

        best_hr = max(best_hr, cur_hr)
        best_ndcg = max(best_ndcg, cur_ndcg)
        if (cur_hr <= best_hr and cur_ndcg <= best_ndcg) or (loss > losses[-1]):
            cur_stop_step += 1
            learning_rate /= 2
        results.append(str(res))

        # 保存最好的模型
        if not config.use_pretrain_model and best_hr <= cur_hr and best_ndcg <= cur_ndcg:
            print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< 保存最好的模型! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
            save_model(model=sgcnhr, path=base_path + '/best_model.pkl')
            print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< 保存嵌入向量和哈希码! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
            saveEmbeddings(sgcnhr, base_path)
            cur_stop_step = 0

            # 数据可视化
            # helper.visualization(sgcnhr.user_embeddings, sgcnhr.item_embeddings, base_path + "/" + str(epoch) + "-visualization.png")
        if cur_stop_step >= early_stop_step:
            print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< 模型触发早停机制! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
            break
    print('best hr and ndcg of user are:', best_hr, best_ndcg)
    save_result(base_path + "/model_logs.txt", results)
    draw_loss(base_path + "/loss_curve.jpg", losses)
    draw_metrics(base_path + '/', hrs, ndcgs, hrs_hash, ndcgs_hash)
    print("Done!")
