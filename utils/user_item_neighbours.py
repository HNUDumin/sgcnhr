import dgl
import networkx as nx
import numpy as np
import pandas as pd
from tqdm import tqdm
import multiprocessing
import os
from scipy.spatial.distance import hamming


def getDataSetInfo(dataset):
    if os.path.exists("../datasets/" + dataset + "/dataInfo.npy"):
        print(dataset, "数据集详情数据文件已存在！")
        return
    train_data_path = "../datasets/" + dataset + "/userRatingTrain.txt"
    test_data_path = "../datasets/" + dataset + "/userRatingTest.txt"
    train_data = []
    test_data = []
    num_user, num_item = 0, 0
    with open(train_data_path, "r") as f:
        for line in tqdm(f.readlines()):
            if line is not None and line != "":
                try:
                    list_ = line.strip('\n').split(" ")
                    list_ = [int(float(i)) for i in list_]
                    num_user = max(num_user, list_[0])
                    num_item = max(num_item, list_[1])
                    train_data.append(list_)
                except Exception:
                    print(line)

    with open(test_data_path, "r") as f:
        for line in tqdm(f.readlines()):
            if line is not None and line != "":
                try:
                    list_ = line.strip('\n').split(" ")
                    list_ = [int(float(i)) for i in list_]
                    num_user = max(num_user, list_[0])
                    num_item = max(num_item, list_[1])
                    test_data.append(list_)
                except Exception:
                    print(line)

    num_item = num_item + 1
    num_user = num_user + 1

    dic = {"num_user": num_user, "num_item": num_item, "train_data": train_data, "test_data": test_data}
    np.save("../datasets/" + dataset + "/dataInfo.npy", dic)
    print("finished: ", dataset)


def getDataSetInfo2(dataset):
    if os.path.exists("../datasets/" + dataset + "/dataInfo.npy"):
        print(dataset, "数据集详情数据文件已存在！")
        return
    train_data_path = "../datasets/" + dataset + "/train.txt"
    test_data_path = "../datasets/" + dataset + "/test.txt"
    train_data = []
    test_data = []
    num_user, num_item = 0, 0
    with open(train_data_path, "r") as f:
        for line in tqdm(f.readlines()):
            if line is not None and line != "":
                try:
                    list_ = line.strip('\n').split(" ")
                    list_ = [int(float(i)) for i in list_]
                    num_user = max(num_user, list_[0])
                    num_item = max(num_item, max(list_[1:]))
                    for item in list_[1:]:
                        train_data.append([list_[0], item, 1])
                except Exception:
                    print(line)

    with open(test_data_path, "r") as f:
        for line in tqdm(f.readlines()):
            if line is not None and line != "":
                try:
                    list_ = line.strip('\n').split(" ")
                    list_ = [int(float(i)) for i in list_]
                    num_user = max(num_user, list_[0])
                    num_item = max(num_item, max(list_[1:]))
                    for item in list_[1:]:
                        test_data.append([list_[0], item, 1])
                except Exception:
                    print(line)

    num_item = num_item + 1
    num_user = num_user + 1

    dic = {"num_user": num_user, "num_item": num_item, "train_data": train_data, "test_data": test_data}
    np.save("../datasets/" + dataset + "/dataInfo.npy", dic)
    print("finished: ", dataset)


def sample_k(k=20, index=None, neighbours=None, user_hash=None, n=500):
    dis = {}
    neighbours = neighbours[:n].copy() if len(neighbours) > n else neighbours
    length = len(neighbours)
    for i in range(length):
        # hamming(): 相同的越多值越小
        dis[neighbours[i]] = hamming(user_hash[index].astype(int), user_hash[neighbours[i]].astype(int))
    dis_sort = np.array(sorted(dis.items(), key=lambda x: x[1]))[:, 0].copy()
    return dis_sort[0:k].copy()


def getNeighboursData(dataset, k=20):
    user_file = "user_neighbours.npy"
    item_file = "item_neighbours.npy"
    if os.path.exists("../datasets/" + dataset + "/" + user_file) and os.path.exists("../datasets/" + dataset + "/" + item_file):
        print(dataset, "邻居节点数据文件已存在！")
        return

    try:
        user_hash = np.load('../datasets/' + dataset + '/userHashCodes.npy')
        item_hash = np.load('../datasets/' + dataset + '/itemHashCodes.npy')
        data = np.load("../datasets/" + dataset + "/dataInfo.npy", allow_pickle=True).item()
    except FileNotFoundError:
        print("哈希码文件不存在！")
        return

    ratings = data['train_data']
    data = pd.DataFrame(ratings, columns=['user', 'item', 'rating'])
    data['user'] = data['user'].astype(int)
    data['item'] = data['item'].astype(int)
    data['item'] = data['item']

    user_list_unique = np.unique(data['user'].values.tolist())
    item_list_unique = np.unique(data['item'].values.tolist())
    num_user = len(user_list_unique)
    num_item = len(item_list_unique)

    user_list_map = dict(zip(range(num_user), user_list_unique))
    user_list_map_reverse = dict(zip(user_list_unique, range(num_user)))
    item_list_map = dict(zip(range(num_user, num_user + num_item), item_list_unique))
    item_list_map_reverse = dict(zip(item_list_unique, range(num_user, num_user + num_item)))

    data['user'] = data['user'].map(user_list_map_reverse)
    data['item'] = data['item'].map(item_list_map_reverse)
    g = dgl.from_networkx(nx.Graph(data[['user', 'item']].values.tolist()))
    print('We have %d nodes.' % g.number_of_nodes())
    print('We have %d edges.' % (g.number_of_edges() // 2))
    res = {}
    for index in tqdm(np.unique(data['user'])):
        temp = dgl.bfs_nodes_generator(g, index)
        if not temp:
            continue
        t = []
        length = len(temp)
        for i in range(length):
            if i in [2, 4, 6]:  # 取三阶邻居
                l = [user_list_map[j.item()] for j in temp[i]]
                t.extend(l)
        if t and len(t) > k:
            t = sample_k(k, user_list_map[index], t, user_hash)
        res[user_list_map[index]] = t
    assert len(set(res.keys()) - set(user_list_unique)) == 0
    np.save("../datasets/" + dataset + "/" + user_file, res)
    res = {}
    for index in tqdm(np.unique(data['item'])):
        temp = dgl.bfs_nodes_generator(g, index)
        if not temp:
            continue
        t = []
        length = len(temp)
        for i in range(length):
            if i in [2, 4, 6]:  # 取三阶邻居
                l = [item_list_map[j.item()] for j in temp[i]]
                t.extend(l)
        if t and len(t) > k:
            t = sample_k(k, item_list_map[index], t, item_hash)
        res[item_list_map[index]] = t
    assert len(set(res.keys()) - set(item_list_unique)) == 0
    np.save("../datasets/" + dataset + "/" + item_file, res)
    print("finished: ", dataset)


if __name__ == '__main__':
    cores = multiprocessing.cpu_count()
    pool = multiprocessing.Pool(1)
    datasets = ['ml-100k', 'book-crossing', 'douban-movie', 'epinions', 'ml-1m', 'amazon-book', 'gowalla']
    pool.map(getDataSetInfo, datasets[0:5])
    pool.map(getDataSetInfo2, datasets[5:])
    pool.map(getNeighboursData, datasets)
    pool.close()
