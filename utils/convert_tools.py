import numpy as np
import scipy.sparse as sp
import tqdm
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.model_selection import train_test_split
from tqdm import tqdm
from concurrent.futures import ProcessPoolExecutor, wait


def convert2inter(dataset):
    _, _, input_data = all_ratings(dataset)
    with open("../datasets/" + dataset + '/' + dataset + ".inter", 'w+') as f:
        f.write('\t'.join(["user_id:token", "item_id:token", "rating:float"]) + '\n')
        for data in tqdm.tqdm(input_data):
            f.write('\t'.join(data) + '\n')


def inter2rating(dataset):
    all_data_path = "../datasets/" + dataset + "/" + dataset + ".inter"
    input_data = []
    with open(all_data_path, "r") as f:
        line = f.readline()
        while line is not None and line != "":
            if "user_id" in line and "item_id" in line:
                line = f.readline()
                continue
            list_ = line.strip('\n').split("\t")[0:3]
            input_data.append(list_)
            line = f.readline()
    input_data = np.array(input_data)
    user_list = sorted(np.unique(input_data[:, 0]))
    item_list = sorted(np.unique(input_data[:, 1]))

    user_dic = dict(zip(user_list, range(len(user_list))))
    item_dic = dict(zip(item_list, range(len(item_list))))

    results = []
    for item in tqdm.tqdm(input_data):
        results.append([user_dic[item[0]], item_dic[item[1]], item[2]])
    train_data, test_data = train_test_split(results, test_size=0.2, random_state=2021)

    np.savetxt('../datasets/' + dataset + '/userRatingTrain.txt', train_data, fmt='%s', delimiter=' ')
    np.savetxt('../datasets/' + dataset + '/userRatingTest.txt', test_data, fmt='%s', delimiter=' ')


def all_ratings(dataset):
    train_data_path = "../datasets/" + dataset + "/userRatingTrain.txt"
    test_data_path = "../datasets/" + dataset + "/userRatingTest.txt"
    input_data = []
    num_user, num_item = 0, 0
    with open(train_data_path, "r") as f:
        line = f.readline()
        while line is not None and line != "":
            list_ = line.strip('\n').split(" ")
            num_user = max(num_user, int(list_[0]))
            num_item = max(num_item, int(list_[1]))
            input_data.append(list_)
            line = f.readline()

    with open(test_data_path, "r") as f:
        line = f.readline()
        while line is not None and line != "":
            list_ = line.strip('\n').split(" ")
            num_user = max(num_user, int(list_[0]))
            num_item = max(num_item, int(list_[1]))
            input_data.append(list_)
            line = f.readline()
    return num_user + 1, num_item + 1, input_data


def cal_users_similarity(dataset):
    num_users, num_items, input_data = all_ratings(dataset)
    print(num_users, num_items)
    mat = sp.lil_matrix((num_users, num_items), dtype=np.float32)
    for item in tqdm.tqdm(input_data):
        user, item, rating = int(item[0]), int(item[1]), float(item[2])
        mat[user, item] = rating
    user_sim = cosine_similarity(mat)
    item_sim = cosine_similarity(mat.transpose())
    np.save('../datasets/' + dataset + '/user_similarity_score.npy', user_sim)
    np.save('../datasets/' + dataset + '/item_similarity_score.npy', item_sim)


K = 20
datasets = ['ml-100k', 'book-crossing', 'douban-movie', 'epinions', 'ml-1m']
tasks = []
executor = ProcessPoolExecutor(max_workers=6)
for dataset in tqdm(datasets):
    tasks.append(executor.submit(cal_users_similarity, dataset))

wait(tasks)
executor.shutdown()
