# encoding:utf-8

import heapq

import numpy as np
import matplotlib.pyplot as plt
from utils import metrics
from scipy.spatial.distance import hamming
import torch
from sklearn.manifold import TSNE
from tqdm import tqdm
import multiprocessing

cores = multiprocessing.cpu_count()


class Helper(object):
    """
    工具类
    """

    def __init__(self, config, dataset):
        self.config = config
        self.dataset = dataset

    def eval_batch_all_rating(self, model, test_users, graph, epoch_id):
        Ks = self.config.topK
        result = {'precision': np.zeros(len(Ks)), 'recall': np.zeros(len(Ks)), 'ndcg': np.zeros(len(Ks)),
                  'hit_ratio': np.zeros(len(Ks)), 'f1': np.zeros(len(Ks)), 'auc': 0.}

        result_hash = {'precision': np.zeros(len(Ks)), 'recall': np.zeros(len(Ks)), 'ndcg': np.zeros(len(Ks)),
                       'hit_ratio': np.zeros(len(Ks)), 'f1': np.zeros(len(Ks)), 'auc': 0.}

        pool = multiprocessing.Pool(cores)
        n_test_users = len(test_users)
        u_batch_size = max(5000, self.config.batch_size)
        n_user_batches = n_test_users // u_batch_size + 1
        count = 0

        for u_batch_id in tqdm(range(n_user_batches), "starting evaluation epoch: {0}".format(str(epoch_id))):
            start = u_batch_id * u_batch_size
            end = (u_batch_id + 1) * u_batch_size
            user_batch = test_users[start: end]
            item_batch = list(range(self.dataset.num_items))
            u_g_embeddings, pos_i_g_embeddings, _ = model(graph, user_batch, item_batch, [])
            user_hash = torch.sign(u_g_embeddings).detach().cpu().numpy().astype(int)
            item_hash = torch.sign(pos_i_g_embeddings).detach().cpu().numpy().astype(int)
            rate_batch_hash = self.hammingSimilarity(user_hash, item_hash)
            rate_batch = model.rating(u_g_embeddings, pos_i_g_embeddings).detach().cpu().numpy()
            assert rate_batch_hash.shape == rate_batch.shape
            user_batch_rating_uid = zip(rate_batch, user_batch)
            user_batch_rating_uid_hash = zip(rate_batch_hash, user_batch)
            batch_result = pool.map(self.test_one_user, user_batch_rating_uid)
            batch_result_hash = pool.map(self.test_one_user, user_batch_rating_uid_hash)
            count += len(batch_result)

            for re in batch_result:
                result['precision'] += re['precision'] / n_test_users
                result['recall'] += re['recall'] / n_test_users
                result['ndcg'] += re['ndcg'] / n_test_users
                result['hit_ratio'] += re['hit_ratio'] / n_test_users
                result['auc'] += re['auc'] / n_test_users
                result['f1'] += re['f1'] / n_test_users

            for re in batch_result_hash:
                result_hash['precision'] += re['precision'] / n_test_users
                result_hash['recall'] += re['recall'] / n_test_users
                result_hash['ndcg'] += re['ndcg'] / n_test_users
                result_hash['hit_ratio'] += re['hit_ratio'] / n_test_users
                result_hash['auc'] += re['auc'] / n_test_users
                result_hash['f1'] += re['f1'] / n_test_users

        assert count == n_test_users
        pool.close()
        return result, result_hash

    def get_auc(self, item_score, user_pos_test):
        item_score = sorted(item_score.items(), key=lambda kv: kv[1])
        item_score.reverse()
        item_sort = [x[0] for x in item_score]
        posterior = [x[1] for x in item_score]

        r = []
        for i in item_sort:
            if i in user_pos_test:
                r.append(1)
            else:
                r.append(0)
        auc = metrics.auc(ground_truth=r, prediction=posterior)
        return auc

    def rank_list_by_sorted(self, user_pos_test, test_items, rating, Ks):
        item_score = {}
        for i in test_items:
            item_score[i] = rating[i]

        K_max = max(Ks)
        K_max_item_score = heapq.nlargest(K_max, item_score, key=item_score.get)

        r = []
        for i in K_max_item_score:
            if i in user_pos_test:
                r.append(1)
            else:
                r.append(0)
        auc = self.get_auc(item_score, user_pos_test)
        return r, auc

    def get_performance(self, user_pos_test, r, auc, Ks):
        precisions, recalls, ndcgs, hit_ratios, f1s = [], [], [], [], []

        for K in Ks:
            precision, recall = metrics.precision_at_k(r, K), metrics.recall_at_k(r, K, len(user_pos_test))
            precisions.append(precision)
            recalls.append(recall)
            ndcgs.append(metrics.ndcg_at_k(r, K))
            hit_ratios.append(metrics.hit_at_k(r, K))
            f1s.append(metrics.F1(precision, recall))

        return {'recall': np.array(recalls), 'precision': np.array(precisions),
                'ndcg': np.array(ndcgs), 'hit_ratio': np.array(hit_ratios),
                'f1': np.array(f1s), 'auc': auc}

    def test_one_user(self, x):
        rating, u = x[0], x[1]
        training_items = self.dataset.train_items[u] if len(self.dataset.train_items[u]) > 0 else []
        user_pos_test = self.dataset.test_items[u]
        all_items = set(range(self.dataset.num_items))
        test_items = list(all_items - set(training_items))
        r, auc = self.rank_list_by_sorted(user_pos_test, test_items, rating, self.config.topK)
        return self.get_performance(user_pos_test, r, auc, self.config.topK)

    def visualization(self, user_data, item_data, filename='visualiztion.png'):
        if isinstance(item_data, dict):
            item_data = [i for i in item_data.values()]
            user_data = [i for i in user_data.values()]
        else:
            item_data = item_data.cpu().detach().numpy()
            user_data = user_data.cpu().detach().numpy()
        tsne = TSNE(n_components=2, init='pca', random_state=0)
        item_result = tsne.fit_transform(item_data)
        user_result = tsne.fit_transform(user_data)
        x_user = []
        x_item = []
        y_user = []
        y_item = []
        for i in range(user_result.shape[0]):
            x_user.append(user_result[i][0])
            y_user.append(user_result[i][1])

        for i in range(item_result.shape[0]):
            x_item.append(item_result[i][0])
            y_item.append(item_result[i][1])
        plt.scatter(x_user, y_user, c='red')
        plt.scatter(x_item, y_item, c='blue')
        plt.xticks()
        plt.yticks()
        plt.title("visualization of embeddings")
        plt.legend()
        plt.savefig(filename)
        plt.close()

    def hammingSimilarity(self, user_hash, item_hash):
        all_res = []
        user_hash_len, item_hash_len = len(user_hash), len(item_hash)
        for j in range(user_hash_len):
            res = []
            for i in range(item_hash_len):
                res.append(1 - hamming(user_hash[j], item_hash[i]))
            all_res.append(res)
        return np.array(all_res)
