# encoding:utf-8
import scipy.sparse as sp
import numpy as np
import torch
from torch.utils.data import TensorDataset, DataLoader
from random import sample
from collections import defaultdict
import dgl


class Dataset(object):
    """
    获取数据集
    """

    def __init__(self, config):
        """
        初始化
        """
        self.config = config
        self.num_negatives = config.num_negatives
        self.k = config.k
        self.num_users, self.num_items, self.train_data, self.test_data = self.get_data_info(config.dataset_path + "dataInfo.npy")
        self.user_trainMatrix = self.load_rating_file_as_matrix()
        self.user_testRatings, self.test_items = self.get_test_ratings()
        # 图数据
        self.user_item_interactions, self.item_user_interactions, self.train_items = self.merge_interaction_data()
        if config.use_neighbours_data > 0:
            self.user_neighbors, self.neighbors_user, self.item_neighbors, self.neighbors_item = self.get_neighbours_interaction(config.user_neighbours_file, config.item_neighbours_file)
        self.create_heterograph()

    def get_neighbours_interaction(self, file_path_user, file_path_item):
        user_data = np.load(file_path_user, allow_pickle=True).item()
        item_data = np.load(file_path_item, allow_pickle=True).item()
        user_neigh = []
        neigh_user = []
        item_neigh = []
        neigh_item = []
        for key in user_data.keys():
            cnt = 0
            for item in user_data[key]:
                cnt = cnt + 1
                if cnt <= self.k:
                    user_neigh.append((int(key), int(item)))
                    neigh_user.append((int(item), int(key)))
                else:
                    break
        for key in item_data.keys():
            for item in item_data[key]:
                cnt = 0
                if cnt <= self.k:
                    item_neigh.append((int(key), int(item)))
                    neigh_item.append((int(item), int(key)))
                else:
                    break
        return user_neigh, neigh_user, item_neigh, neigh_item

    def get_train_ratings(self):
        user_item = []
        item_user = []
        train_items = defaultdict(list)
        for item in self.train_data:
            user, item = int(item[0]), int(item[1])
            user_item.append((user, item))
            item_user.append((item, user))
            train_items[user].append(item)
        return user_item, item_user, train_items

    def merge_interaction_data(self):
        user_item_train, item_user_train, train_items = self.get_train_ratings()
        return user_item_train, item_user_train, train_items

    def get_test_ratings(self):
        rating_list = []
        test_items = defaultdict(list)
        for arr in self.test_data:
            user, item = int(arr[0]), int(arr[1])
            rating_list.append([user, item])
            test_items[user].append(item)
        return rating_list, test_items

    def get_data_info(self, file_path):
        data = np.load(file_path, allow_pickle=True).item()
        return data['num_user'], data['num_item'], data['train_data'], data['test_data']

    def load_rating_file_as_matrix(self):
        mat = sp.dok_matrix((self.num_users, self.num_items), dtype=np.float32)
        for arr in self.train_data:
            if len(arr) > 2:
                user, item, rating = int(arr[0]), int(arr[1]), float(arr[2])
                if rating > 0:
                    mat[user, item] = 1.0
            else:
                user, item = int(arr[0]), int(arr[1])
                mat[user, item] = 1.0
        return mat

    def get_train_instances(self, train):
        user_input, pos_item_input, neg_item_input = [], [], []
        for (u, i) in train.keys():
            neg_items = sample(list(set(range(self.num_items)) - set(self.train_items[u])), self.num_negatives)
            for item in neg_items:
                pos_item_input.append(i)
                user_input.append(u)
                neg_item_input.append(item)
        pi_ni = [[pi, ni] for pi, ni in zip(pos_item_input, neg_item_input)]
        return user_input, pi_ni

    def get_user_data_loader(self, batch_size):
        user, pos_neg_item_at_u = self.get_train_instances(self.user_trainMatrix)
        train_data = TensorDataset(torch.LongTensor(user).to(self.config.gpu), torch.LongTensor(pos_neg_item_at_u).to(self.config.gpu))
        user_train_loader = DataLoader(train_data, batch_size=batch_size, shuffle=True)
        return user_train_loader

    def create_heterograph(self):
        if self.config.use_neighbours_data == 0:
            self.graph = dgl.heterograph({
                ('user', 'u_i', 'item'): self.user_item_interactions,
                ('item', 'i_u', 'user'): self.item_user_interactions
            })
        elif self.config.use_neighbours_data == 1:
            self.graph = dgl.heterograph({
                ('user', 'u_i', 'item'): self.user_item_interactions,
                ('item', 'i_u', 'user'): self.item_user_interactions,
                ('item', 'i_n', 'item'): self.item_neighbors,
                ('item', 'n_i', 'item'): self.neighbors_item
            })
        elif self.config.use_neighbours_data == 2:
            self.graph = dgl.heterograph({
                ('user', 'u_i', 'item'): self.user_item_interactions,
                ('item', 'i_u', 'user'): self.item_user_interactions,
                ('user', 'u_n', 'user'): self.user_neighbors,
                ('user', 'n_u', 'user'): self.neighbors_user

            })
        elif self.config.use_neighbours_data == 3:
            self.graph = dgl.heterograph({
                ('user', 'u_i', 'item'): self.user_item_interactions,
                ('item', 'i_u', 'user'): self.item_user_interactions,
                ('user', 'u_n', 'user'): self.user_neighbors,
                ('user', 'n_u', 'user'): self.neighbors_user,
                ('item', 'i_n', 'item'): self.item_neighbors,
                ('item', 'n_i', 'item'): self.neighbors_item
            })
        print('We have %d nodes.' % self.graph.number_of_nodes())
        print('We have %d edges.' % (self.graph.number_of_edges() // 2))
